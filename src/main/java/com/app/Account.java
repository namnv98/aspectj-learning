package com.app;

public class Account {
    public String typeAccount;
    private String fullName;
    private Integer amountTotal;
    public Account() {
    }
    public Account(String fullName, Integer amountTotal) {
        this.fullName = fullName;
        this.amountTotal = amountTotal;
    }

    public boolean withdrawal(Integer amount) {
        if (amountTotal < amount && amountTotal < 50000) {
            return false;
        }
        try {
            amountTotal = amountTotal - amount;
            return true;
        } catch (Exception ex) {
            return false;
        }

    }

    public boolean deposit(Integer amount) {
        try {
            amountTotal = amountTotal + amount;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}