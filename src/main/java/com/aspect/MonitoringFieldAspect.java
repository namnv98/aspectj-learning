package com.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class MonitoringFieldAspect {
    @Pointcut("get(* com.app.Account.typeAccount)")
    public void fieldGet() {
    }

    @AfterReturning(pointcut = "fieldGet()", returning = "fieldValue")
    public void afterFieldAccess(Object fieldValue, JoinPoint thisJoinPoint) {
        System.out.println("get value " + thisJoinPoint.getSignature().getName() + " = " + fieldValue);
    }

    @Pointcut("set(* com.app.Account.typeAccount)")
    public void fieldSet() {
    }

    @After("fieldSet()")
    public void afterFieldAccess(JoinPoint thisJoinPoint) {
        System.out.println("set value " + thisJoinPoint.getSignature().getName() + " = " + thisJoinPoint.getArgs()[0]);
    }
}
