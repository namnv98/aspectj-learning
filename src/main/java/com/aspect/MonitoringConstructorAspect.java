package com.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.util.Arrays;

@Aspect
public class MonitoringConstructorAspect {

    @Pointcut("execution(com.app.Account.new(..))")
    public void executionConstructor() {
    }

    @After("executionConstructor()")
    public void execution(JoinPoint thisJoinPoint) {
        System.out.println("Execution Constructor Account:" + thisJoinPoint.getSignature().getName() + " = " + Arrays.toString(thisJoinPoint.getArgs()));
    }

    @Pointcut("initialization(com.app.Account.new(..))")
    public void initializationConstructor() {
    }

    @After("initializationConstructor()")
    public void initialization(JoinPoint thisJoinPoint) {
        System.out.println("Initialization Constructor Account:" + thisJoinPoint.getSignature().getName() + " = " + Arrays.toString(thisJoinPoint.getArgs()));
    }

    @Pointcut("call(com.app.Account.new(..))")
    public void callConstructor() {
    }

    @After("callConstructor()")
    public void call(JoinPoint thisJoinPoint) {
        System.out.println("Call Constructor Account:" + thisJoinPoint.getSignature().getName() + " = " + Arrays.toString(thisJoinPoint.getArgs()));
    }
}
