package com.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.lang.reflect.Field;

@Aspect
public class MonitoringMethodAspect {
    private String name;

    @Pointcut("execution(* com.app.Account.*(..))")
    public void callAll() {
    }

    @Pointcut("execution(* com.app.Account.withdrawal(..))")
    public void callWithdrawal() {
    }

    @Pointcut("execution(* com.app.Account.deposit(..))")
    public void callDeposit() {
    }

    @AfterReturning(value = "callWithdrawal()", returning = "returnValue")
    public void afterWithdrawal(JoinPoint pjp, Object returnValue) throws Throwable {
        System.out.println("Rut tien: " + (boolean) returnValue);
    }

    @Before("callWithdrawal()")
    public void beforeWithdrawal(JoinPoint joinPoint) throws Throwable {
        Object instance = joinPoint.getThis();
        Field privateUriField = instance.getClass().getDeclaredField("fullName");
        privateUriField.setAccessible(true);
        name = (String) privateUriField.get(instance);
        System.out.println(name + ": Rut tien: " + joinPoint.getArgs()[0] + "VND");
    }

    @AfterReturning(value = "callDeposit()", returning = "returnValue")
    public void afterDeposit(JoinPoint pjp, Object returnValue) throws Throwable {
        System.out.println("Nap tien: " + (boolean) returnValue);
    }

    @Before("callDeposit()")
    public void beforeDeposit(JoinPoint joinPoint) throws Throwable {
        Object instance = joinPoint.getThis();
        Field privateUriField = instance.getClass().getDeclaredField("fullName");
        privateUriField.setAccessible(true);
        name = (String) privateUriField.get(instance);
        System.out.println(name + ": Nap tien: " + joinPoint.getArgs()[0] + "VND");
    }

    @Around("callAll()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("dang thuc hien giao dich");
        return pjp.proceed();
    }
}
