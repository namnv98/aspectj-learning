package com.app;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class MonitoringMethodAspectTest {
    private Account account;

    @Before
    public void before() {
        account = new Account("Nguyen Hoang Anh", 0);
    }

    @Test
    public void withdrawal_thenSuccess() {
        assertFalse(account.withdrawal(5));
    }

    @Test
    public void deposit_thenFail() {
        assertTrue(account.deposit(100));
    }
}
