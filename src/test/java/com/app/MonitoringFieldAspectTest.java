package com.app;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class MonitoringFieldAspectTest {
    private Account account;

    @Before
    public void before() {
        account = new Account();
    }

    @Test
    public void withdrawal_thenSuccess() {
        String type = "Account Bank";
        account.typeAccount = type;
        assertTrue(account.typeAccount.equals(type));
    }


}
